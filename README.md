When you scan several photos at a time, you then have to extract each photo from the image. Usually they are slightly rotated, so you must first crop a bigger rectangle around each photo, rotate each image and make the final fine crop. This tool helps you do this on several images at a time. 

The documentation is in the doc folder.

