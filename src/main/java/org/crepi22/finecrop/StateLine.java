package org.crepi22.finecrop;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * @author pierre
 * Define a horizontal line of states to show the progression of the operations
 */

public class StateLine extends JPanel {

    private static final long serialVersionUID = 1L;
    
    private JLabel[] states;
    private int state = -1;
    private Color disabledColor = Color.LIGHT_GRAY;
    private Color enabledColor = Color.DARK_GRAY;
    final Border disabledBorder = 
            BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black, 1), BorderFactory.createEmptyBorder(2, 6, 2, 6));
    // new LineBorder(Color.black, 1, true);
    final Border enabledBorder = 
            BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.white, 1), BorderFactory.createEmptyBorder(2, 6, 2, 6));
    // new LineBorder(Color.white, 1, true);

    /**
     * Constructor with all the potential states
     * @param stateLabels labels for the states
     * @param tooltips tooltips. The array may be null.
     */
    public StateLine(List<String> stateLabels, List<String> tooltips) {
        setOpaque(true);
        setBackground(disabledColor);
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(layout);
        int l = stateLabels.size();
        states = new JLabel[l];
        for(int i = 0; i < l; i++) {
            JLabel state = new JLabel(stateLabels.get(i));
            state.setBorder(disabledBorder);
            state.setOpaque(true);
            state.setBackground(disabledColor);
            if (tooltips != null) state.setToolTipText(tooltips.get(i));
            states[i] = state; 
            if (i > 0) add(Box.createHorizontalGlue());
            add(state);
        }
    }
    
    /**
     * @param i
     */
    public void setState(int i) {
        if (state >=0) {
            states[state].setBackground(disabledColor);
            states[state].setBorder(disabledBorder);
            states[state].setForeground(Color.BLACK);
        }
        state = i;
        if (state >=0) {
            states[state].setBackground(enabledColor);
            states[state].setBorder(enabledBorder);
            states[state].setForeground(Color.WHITE);
        }
    }
    
}
