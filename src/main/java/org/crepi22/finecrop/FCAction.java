
package org.crepi22.finecrop;

/**
 * @author pierre The sequence of actions to apply on an image. Each action has
 *         a continuation.
 */
public class FCAction {

    /** The Constant ROTATE : fine rotation of an image. */
    public static final int ROTATE = 1;

    /** The Constant CROP : crop the borders of an image. */
    public static final int CROP = 2;

    /** The Constant MULTICROP : cut an image in multiple segments. */
    public static final int MULTICROP = 3;

    /** The Constant CONFIRM : last step to confirm an operation. */
    public static final int CONFIRM = 4;

    /** The semantics of the action. */
    final public int kind;

    /**
     * The continuation of the action (what is performed on the file after). The
     * continuation is null if there is nothing else to perform
     */
    final public FCAction continuation;

    final static FCAction BOOK_MODE = new FCAction(ROTATE, new FCAction(CROP, new FCAction(CONFIRM, null)));

    final static FCAction CUT_MODE = new FCAction(MULTICROP, new FCAction(CONFIRM, null));

    final static FCAction PHOTO_MODE = new FCAction(MULTICROP, FCAction.BOOK_MODE);

    /**
     * Instantiates a new action.
     * 
     * @param kind the type of action
     * @param continuation the continuation (may be null)
     */
    public FCAction(int kind, FCAction continuation) {
        this.kind = kind;
        this.continuation = continuation;
    }
}
