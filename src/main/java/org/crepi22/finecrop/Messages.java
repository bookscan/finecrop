
package org.crepi22.finecrop;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author pierre
 * Internationalization class.
 */
public class Messages {
    private static final String BUNDLE_NAME = "org.crepi22.finecrop.messages"; //$NON-NLS-1$

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Messages() {
    }

    /**
     * Internationalization.
     * @param key
     * @return the text in hopefully the right language.
     */
    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
