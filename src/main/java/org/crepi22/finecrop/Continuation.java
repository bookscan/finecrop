
package org.crepi22.finecrop;

import java.io.File;
import java.util.List;

/**
 * @author pierre
 * Controls provided by the controller to the photoView so that it can notify the results of its actions.
 */
public interface Continuation {

    /**
     * Register a newly generated file for further processing
     * @param file
     */
    public abstract void runFile(File file);

    /**
     * Register a list of newly generated files for further processing
     * @param file
     */
    public abstract void runFiles(List<File> file);

    /**
     * Take notice that the operation is cancelled.
     */
    public void cancel();

    /**
     * Show a blur effect that disable the processing of events while a long operation is performed.
     * @param b
     */
    public void showEffect(boolean b);

    /**
     * Display an error message
     * @param string
     */
    public  void error(String string);

}
