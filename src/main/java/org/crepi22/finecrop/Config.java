package org.crepi22.finecrop;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * The application settings.
 */
public class Config {
    private static final String CONVERT_KEY = "convert"; //$NON-NLS-1$
    static Preferences prefs = Preferences.userNodeForPackage(Config.class);
    final static String DefaultConvert;
    
    static {
        switch(OS.getOS()) {
            case Windows:
                DefaultConvert="C:\\Program Files\\ImageMagick\\convert.exe"; //$NON-NLS-1$
                break;
            default:
                DefaultConvert="/usr/bin/convert"; //$NON-NLS-1$
        }
    }
    
    /**
     * Gets the path to convert executable.
     *
     * @return the convert path
     */
    static public String getConvertPath() {
        return prefs.get(CONVERT_KEY,DefaultConvert);
    }
    
    /**
     * Sets the path to convert executable.
     *
     * @param path the new convert path
     * @throws BackingStoreException if backing store not available (disk full ?)
     */
    static public void setConvertPath(String path) throws BackingStoreException {
        prefs.put(CONVERT_KEY,path);
        prefs.flush();
    }
}
