package org.crepi22.finecrop;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * @author pierre
 * A glass pane to cover the frame of the application that gives a translucent effect showing the processing is
 * disabled while performing image magick operations.
 */
public class TranslucentGlassPane extends JPanel {
    
    private static final long serialVersionUID = 1L;

    /**
     * Translucent pane.
     */
    public TranslucentGlassPane() {
        super();
        setOpaque(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        if(isEnabled()) {
            g.setColor(new Color(0.5f,0.5f,0.5f,0.3f));
            g.fillRect(0, 0, w, h);
        }
    }
}
